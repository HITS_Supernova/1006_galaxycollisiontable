
{//websockets
// 	var socket = new WebSocket('ws://echo.websocket.org');
	var socket = new WebSocket('ws://localhost:8881/ws');//8888 taken by the heartbeat
}

{//websockets result output div visibility
	var websocketdiv1 = document.getElementById('result');
// 	websocketdiv1.style.visibility = 'visible';
	websocketdiv1.style.visibility = 'hidden';
	
	var websocketdiv2 = document.getElementById('result2');
// 	websocketdiv2.style.visibility = 'visible';
	websocketdiv2.style.visibility = 'hidden';	
}

{//other parameters
	//websockets
// 	var particles_in_one_galaxy = 44444;
// 	var particles_per_player_limit = 2*particles_in_one_galaxy;
	var particles_per_player_limit = 50000;//50000;
	
	//two velocity parameters (slowdownfactor, velocity_scale)
	var slowdownfactor = 30;//6 //applied to the pad velocity itself 
	var velocity_scale = 2.0;//0.1;//13.0;//just transfered to websockets output upon release	
	
	//no releasing within this distance from the menu corner
	var menu_distance_measure = 550.;
	
	//(text bars) setInterval parameter for reverting to the default text
	var defaultTextInterval = 25000;
	
	//timeOut parameter (miliseconds) for getting the current number of particles in the text
	var timeOutParameter = 125;
	
	//should mouse event listeners be used (can cause "double clicks" on touch screens)
	var MEL = false;
}

{//pad images (12 galaxies)
	var padImageEmpty = 'Images/MenuBase/icon_empty.png';  
  
	var menuImagesFolder = "Images/MenuImages/";
	
	var padImage1 = menuImagesFolder+'G1_pad.png';
	var padImage2 = menuImagesFolder+'G2_pad.png';
	var padImage3 = menuImagesFolder+'G3_pad.png';	
	var padImage4 = menuImagesFolder+'G4_pad.png';
	var padImage5 = menuImagesFolder+'G5_pad.png';
	var padImage6 = menuImagesFolder+'G6_pad.png';
	var padImage7 = menuImagesFolder+'G7_pad.png';
	var padImage8 = menuImagesFolder+'G8_pad.png';	
	var padImage9 = menuImagesFolder+'G9_pad.png';
	var padImage10 = menuImagesFolder+'G10_pad.png';	
	var padImage11 = menuImagesFolder+'G11_pad.png';
	var padImage12 = menuImagesFolder+'G12_pad.png';	
	
	var menuImages1PadIcons = [padImage1, padImage2, padImage3, padImage4, padImage5, padImage6, padImage7, padImage8, padImage9, padImage10, padImage11, padImage12];  
	var menuImages2PadIcons = [padImage1, padImage2, padImage3, padImage4, padImage5, padImage6, padImage7, padImage8, padImage9, padImage10, padImage11, padImage12];
	var menuImages3PadIcons = [padImage1, padImage2, padImage3, padImage4, padImage5, padImage6, padImage7, padImage8, padImage9, padImage10, padImage11, padImage12];
	var menuImages4PadIcons = [padImage1, padImage2, padImage3, padImage4, padImage5, padImage6, padImage7, padImage8, padImage9, padImage10, padImage11, padImage12];	
}

{//rotating menu images and text
	//rotating menu:
	//4 corners 		-> m1, m2, m3, m4
	//3 button positions	-> p, a, n (previous, active, next)
	//12x galaxies 		-> G1,... ,G12 -> 12x pads 
	//menu images		-> 12x menu images per galaxy (4 corners in 3 positions - previous, active, next)
	//				naming: e.g. 	G1_m1a.png -> galaxy 1 (G1), corner 1 (m1), active (a)
	//						G7_m3n.png -> galaxy 7 (G7), corner 3 (m3), next (n)
	//			-> 12 galaxies times 12 images per galaxy = 144 menu images in total	-> 36 images per corner (player)
	
	
	//main menu images
	var menuImage1Base =  "/MenuBase/Menu1.png";
	var menuImage2Base =  "/MenuBase/Menu2.png";
	var menuImage3Base =  "/MenuBase/Menu3.png";
	var menuImage4Base =  "/MenuBase/Menu4.png";
	var menuImage1BaseGlow =  "/MenuBase/Menu1g3.png";
	var menuImage2BaseGlow =  "/MenuBase/Menu2g3.png";
	var menuImage3BaseGlow =  "/MenuBase/Menu3g3.png";
	var menuImage4BaseGlow =  "/MenuBase/Menu4g3.png";
	
	//filling the menu image arrays containg the 12 galaxies to choose from
	//previous button is decreasing the number (G1, G2, ...)
// 	var menuImages1Previous = ["/G1/G1_m1p.png","/G2/G2_m1p.png","/G3/G3_m1p.png","/G4/G4_m1p.png","/G5/G5_m1p.png", "/G6/G6_m1p.png","/G7/G7_m1p.png","/G8/G8_m1p.png","/G9/G9_m1p.png","/G10/G10_m1p.png","/G11/G11_m1p.png","/G12/G12_m1p.png"];//initial G2
// 	var menuImages1Active = ["/G1/G1_m1a.png","/G2/G2_m1a.png","/G3/G3_m1a.png","/G4/G4_m1a.png","/G5/G5_m1a.png", "/G6/G6_m1a.png","/G7/G7_m1a.png","/G8/G8_m1a.png","/G9/G9_m1a.png","/G10/G10_m1a.png","/G11/G11_m1a.png","/G12/G12_m1a.png"];
// 	var menuImages1Next = ["/G1/G1_m1n.png","/G2/G2_m1n.png","/G3/G3_m1n.png","/G4/G4_m1n.png","/G5/G5_m1n.png", "/G6/G6_m1n.png","/G7/G7_m1n.png","/G8/G8_m1n.png","/G9/G9_m1n.png","/G10/G10_m1n.png","/G11/G11_m1n.png","/G12/G12_m1n.png"];
	var menuImages1Previous = ["/G12/G12_m1p.png", "/G1/G1_m1p.png","/G2/G2_m1p.png","/G3/G3_m1p.png","/G4/G4_m1p.png","/G5/G5_m1p.png", "/G6/G6_m1p.png","/G7/G7_m1p.png","/G8/G8_m1p.png","/G9/G9_m1p.png","/G10/G10_m1p.png","/G11/G11_m1p.png"];//initial G1
	var menuImages1Active = ["/G12/G12_m1a.png", "/G1/G1_m1a.png","/G2/G2_m1a.png","/G3/G3_m1a.png","/G4/G4_m1a.png","/G5/G5_m1a.png", "/G6/G6_m1a.png","/G7/G7_m1a.png","/G8/G8_m1a.png","/G9/G9_m1a.png","/G10/G10_m1a.png","/G11/G11_m1a.png"];
	var menuImages1Next = ["/G12/G12_m1n.png", "/G1/G1_m1n.png","/G2/G2_m1n.png","/G3/G3_m1n.png","/G4/G4_m1n.png","/G5/G5_m1n.png", "/G6/G6_m1n.png","/G7/G7_m1n.png","/G8/G8_m1n.png","/G9/G9_m1n.png","/G10/G10_m1n.png","/G11/G11_m1n.png"];	
	
	var menuImages2Previous = ["/G1/G1_m2p.png","/G2/G2_m2p.png","/G3/G3_m2p.png","/G4/G4_m2p.png","/G5/G5_m2p.png", "/G6/G6_m2p.png","/G7/G7_m2p.png","/G8/G8_m2p.png","/G9/G9_m2p.png","/G10/G10_m2p.png","/G11/G11_m2p.png","/G12/G12_m2p.png"];//initial G2
	var menuImages2Active = ["/G1/G1_m2a.png","/G2/G2_m2a.png","/G3/G3_m2a.png","/G4/G4_m2a.png","/G5/G5_m2a.png", "/G6/G6_m2a.png","/G7/G7_m2a.png","/G8/G8_m2a.png","/G9/G9_m2a.png","/G10/G10_m2a.png","/G11/G11_m2a.png","/G12/G12_m2a.png"];
	var menuImages2Next = ["/G1/G1_m2n.png","/G2/G2_m2n.png","/G3/G3_m2n.png","/G4/G4_m2n.png","/G5/G5_m2n.png", "/G6/G6_m2n.png","/G7/G7_m2n.png","/G8/G8_m2n.png","/G9/G9_m2n.png","/G10/G10_m2n.png","/G11/G11_m2n.png","/G12/G12_m2n.png"];
		
// 	var menuImages3Previous = ["/G1/G1_m3p.png","/G2/G2_m3p.png","/G3/G3_m3p.png","/G4/G4_m3p.png","/G5/G5_m3p.png", "/G6/G6_m3p.png","/G7/G7_m3p.png","/G8/G8_m3p.png","/G9/G9_m3p.png","/G10/G10_m3p.png","/G11/G11_m3p.png","/G12/G12_m3p.png"];//initial G2
// 	var menuImages3Active = ["/G1/G1_m3a.png","/G2/G2_m3a.png","/G3/G3_m3a.png","/G4/G4_m3a.png","/G5/G5_m3a.png", "/G6/G6_m3a.png","/G7/G7_m3a.png","/G8/G8_m3a.png","/G9/G9_m3a.png","/G10/G10_m3a.png","/G11/G11_m3a.png","/G12/G12_m3a.png"];
// 	var menuImages3Next = ["/G1/G1_m3n.png","/G2/G2_m3n.png","/G3/G3_m3n.png","/G4/G4_m3n.png","/G5/G5_m3n.png", "/G6/G6_m3n.png","/G7/G7_m3n.png","/G8/G8_m3n.png","/G9/G9_m3n.png","/G10/G10_m3n.png","/G11/G11_m3n.png","/G12/G12_m3n.png"];
	var menuImages3Previous = ["/G2/G2_m3p.png","/G3/G3_m3p.png","/G4/G4_m3p.png","/G5/G5_m3p.png", "/G6/G6_m3p.png","/G7/G7_m3p.png","/G8/G8_m3p.png","/G9/G9_m3p.png","/G10/G10_m3p.png","/G11/G11_m3p.png","/G12/G12_m3p.png", "/G1/G1_m3p.png"];//initial G3
	var menuImages3Active = ["/G2/G2_m3a.png","/G3/G3_m3a.png","/G4/G4_m3a.png","/G5/G5_m3a.png", "/G6/G6_m3a.png","/G7/G7_m3a.png","/G8/G8_m3a.png","/G9/G9_m3a.png","/G10/G10_m3a.png","/G11/G11_m3a.png","/G12/G12_m3a.png", "/G1/G1_m3a.png"];
	var menuImages3Next = ["/G2/G2_m3n.png","/G3/G3_m3n.png","/G4/G4_m3n.png","/G5/G5_m3n.png", "/G6/G6_m3n.png","/G7/G7_m3n.png","/G8/G8_m3n.png","/G9/G9_m3n.png","/G10/G10_m3n.png","/G11/G11_m3n.png","/G12/G12_m3n.png", "/G1/G1_m3n.png"];	
	
// 	var menuImages4Previous = ["/G1/G1_m4p.png","/G2/G2_m4p.png","/G3/G3_m4p.png","/G4/G4_m4p.png","/G5/G5_m4p.png", "/G6/G6_m4p.png","/G7/G7_m4p.png","/G8/G8_m4p.png","/G9/G9_m4p.png","/G10/G10_m4p.png","/G11/G11_m4p.png","/G12/G12_m4p.png"];//initial G2
// 	var menuImages4Active = ["/G1/G1_m4a.png","/G2/G2_m4a.png","/G3/G3_m4a.png","/G4/G4_m4a.png","/G5/G5_m4a.png", "/G6/G6_m4a.png","/G7/G7_m4a.png","/G8/G8_m4a.png","/G9/G9_m4a.png","/G10/G10_m4a.png","/G11/G11_m4a.png","/G12/G12_m4a.png"];
// 	var menuImages4Next = ["/G1/G1_m4n.png","/G2/G2_m4n.png","/G3/G3_m4n.png","/G4/G4_m4n.png","/G5/G5_m4n.png", "/G6/G6_m4n.png","/G7/G7_m4n.png","/G8/G8_m4n.png","/G9/G9_m4n.png","/G10/G10_m4n.png","/G11/G11_m4n.png","/G12/G12_m4n.png"];		
	var menuImages4Previous = ["/G3/G3_m4p.png","/G4/G4_m4p.png","/G5/G5_m4p.png", "/G6/G6_m4p.png","/G7/G7_m4p.png","/G8/G8_m4p.png","/G9/G9_m4p.png","/G10/G10_m4p.png","/G11/G11_m4p.png", "/G12/G12_m4p.png", "/G1/G1_m4p.png","/G2/G2_m4p.png"];//initial G4
	var menuImages4Active = ["/G3/G3_m4a.png","/G4/G4_m4a.png","/G5/G5_m4a.png", "/G6/G6_m4a.png","/G7/G7_m4a.png","/G8/G8_m4a.png","/G9/G9_m4a.png","/G10/G10_m4a.png","/G11/G11_m4a.png", "/G12/G12_m4a.png", "/G1/G1_m4a.png","/G2/G2_m4a.png"];
	var menuImages4Next = ["/G3/G3_m4n.png","/G4/G4_m4n.png","/G5/G5_m4n.png", "/G6/G6_m4n.png","/G7/G7_m4n.png","/G8/G8_m4n.png","/G9/G9_m4n.png","/G10/G10_m4n.png","/G11/G11_m4n.png", "/G12/G12_m4n.png", "/G1/G1_m4n.png","/G2/G2_m4n.png"];			
	
	
	//text for the 12 galaxies (rotating menu)
	var menuImages1Text = [//No. 
	"1: Large Elliptical Galaxy (24879 particles)",				//11336 dark matter particles, 13543 star particles, Total number of particles (dm + stars): 24879
	"2: Large Spiral Galaxy with companions (27368 particles)",		//11499 dark matter particles, 15869 star particles, Total number of particles (dm + stars): 27368
	"3: Large Spiral Galaxy (30425 particles)",				//12390 dark matter particles, 18035 star particles, Total number of particles (dm + stars): 30425
	"4: Large Spiral Galaxy (22181 particles)",				//9902 dark matter particles, 12279 star particles, Total number of particles (dm + stars): 22181
	"5: Middle-sized Disk Galaxy (13034 particles)",			//7311 dark matter particles, 5723 star particles, Total number of particles (dm + stars): 13034
	"6: Middle-sized Disk Galaxy with several companions (13211 paricles)",	//7325 dark matter particles, 5886 star particles, Total number of particles (dm + stars): 13211
	"7: Middle-sized Disk Galaxy with a companion (14044 particles)",	//8066 dark matter particles, 5978 star particles, Total number of particles (dm + stars): 14044
	"8: Small Disk Galaxy (6575 particles)",				//4602 dark matter particles, 1973 star particles, Total number of particles (dm + stars): 6575
	"9: Small Disk Galaxy (6679 particles)",				//4714 dark matter particles, 1965 star particles, Total number of particles (dm + stars): 6679
	"10: Small Disk Galaxy with a companion (6627 particles)",		//4876 dark matter particles, 1751 star particles, Total number of particles (dm + stars): 6627
	"11: Small Eliptical Galaxy (6233 particles)",				//4150 dark matter particles, 2083 star particles, Total number of particles (dm + stars): 6233
	"12: Tiny Disk Galaxy (3351 particles)" 				//2802 dark matter particles, 549 star particles, Total number of particles (dm + stars): 3351	
	];
	
	var menuImages1TextDE = [//Nr. 
	"1: Große Elliptische Galaxie (24879 Teilchen)",
	"2: Große Scheibengalaxie mit Begleitern (27368 Teilchen)",
	"3: Große Scheibengalaxie (30425 Teilchen)",
	"4: Große Scheibengalaxie (22181 Teilchen)",
	"5: Mittelgroße Scheibengalaxie (13034 Teilchen)",
	"6. Mittelgroße Scheibengalaxie mit mehreren Begleitern (13211 Teilchen)",
	"7: Mittelgroße Scheibengalaxie mit einem Begleiter (14044 Teilchen)",
	"8: Kleine Scheibengalaxie (6575 Teilchen)",
	"9: Kleine Scheibengalaxie (6679 Teilchen)",
	"10: Kleine Scheibengalaxie mit einem Begleiter (6627 Teilchen)",
	"11: Kleine Elliptische Galaxie (6233 Teilchen)",
	"12: Winzige Scheibengalaxie (3351 Teilchen)"
	];

	var menuImages2Text = menuImages1Text;
	var menuImages2TextDE = menuImages1TextDE;
	var menuImages3Text = menuImages1Text;
	var menuImages3TextDE = menuImages1TextDE;	
	var menuImages4Text = menuImages1Text;
	var menuImages4TextDE = menuImages1TextDE;			
}

{//tex bar text strings
	{//default text 						(Push the Galaxy in!) on start (centered)		(Push the Galaxy in! You are using X particles!) on idle
	//used just in the beginning in UpdateMenu()
		var svg_text_default = "";
		//ugly hack
		svg_text_default += "iiii";
		svg_text_default += "i i&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_default += "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_default += "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_default += "&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;";
		svg_text_default += "Push the Galaxy in!";	
		
		var svg_text_defaultDE = "";
		//ugly hack
		svg_text_defaultDE += "iiiioo";
		svg_text_defaultDE += "i i&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_defaultDE += "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_defaultDE += "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
		svg_text_defaultDE += "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;";
		svg_text_defaultDE += "Bewegen sie die Galaxie!";			
	
	//used in IntervalFunctionX() - idle 					(Push the Galaxy in! You are using X particles!)
		var svg_text_defaultB_EN = "Push the Galaxy in!";
		var svg_text_defaultB_DE = "Bewegen Sie die Galaxie!";
	}

	{//locked menu text						(Too Many Particles - You are using X particles!)
	//used in InsertPredefinedMenuText in BlockCheck()
		var svg_text_locked_en = "Too Many Particles -";
		var svg_text_locked_de = "Zu viele Teilchen -";
		//websockets div text
		var html_block_text_1 = '<li class="received"><span>TOO MUCH PARTICLES PLAYER 1!!!</span></li>';
		var html_block_text_2 = '<li class="received"><span>TOO MUCH PARTICLES PLAYER 2!!!</span></li>';
		var html_block_text_3 = '<li class="received"><span>TOO MUCH PARTICLES PLAYER 3!!!</span></li>';
		var html_block_text_4 = '<li class="received"><span>TOO MUCH PARTICLES PLAYER 4!!!</span></li>';
	}
	
	//buffers to keep the spacing (used with default, locked, rotating menu text)
	var svg_text_buffer_en = "                                                                          ";//font-size=5
	var svg_text_buffer_de = "                                                                                ";	//font-size=5	
	
	{//info menu text (with its own buffers)			(Push the Galaxy in! You are using X particles!) same as idle		or (You are using X particles!)
	//InsertPredefinedMenuText(player, svg_text_buffer_info_en, svg_text_buffer_info_de, svg_text_info_EN, svg_text_info_DE);
		var svg_text_buffer_info_en = "                                                                          ";//font-size=5
		var svg_text_buffer_info_de = "                                                                                ";	//font-size=5	
		var svg_text_info_EN = "";//"Move the pad outside the dock."; 			//to release a galaxy.
		var svg_text_info_DE = "";//"Bewegen Sie das Pad ausserhalb des Docks.";	//, um eine Galaxie freizugeben.
	}
	
	//addons for locked and info texts (after the number of particles used)
	var text_init_info = " You are using ";//"  ";
	var text_init_info_de = " Sie benutzen ";//"  ";
	var svg_text_particle_EN = " particles!";
	var svg_text_particle_DE = " Teilchen!";
}
