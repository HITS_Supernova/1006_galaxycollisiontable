		//websockets
		var messagesListSent = document.getElementById('messagesSent');
		var messagesListReceived = document.getElementById('messagesReceived');
		var messagesListReceivedParticleNumber = document.getElementById('messagesReceivedParticleNumber');
		var socketStatus = document.getElementById('status');
		var closeBtn = document.getElementById('close');
		
		// Create a new WebSocket.
		//socket defined in parameters.js
// 		var socket;
		var object;//need to have the particle numbers in the code
	
		function WebsocketStart(){
			// Handle any errors that occur.
			socket.onerror = function(error) {
				console.log('WebSocket Error: ' + error);
			};

			// Show a connected message when the WebSocket is opened.
			socket.onopen = function(event) {
				socketStatus.innerHTML = 'Connected to: ' + event.currentTarget.url;
				socketStatus.className = 'open';
				console.log(event.currentTarget);
				console.log(event.currentTarget.url);
			};

			// Handle messages sent by the server.
			socket.onmessage = function(event) {//report (block if too many particles)
				var message = event.data;
				messagesReceived.innerHTML = '<li class="received"><span>Received:</span>' + message + '</li>';			    
				
				object = JSON.parse(message);
//     				console.log("websockets", object);
				
				//user_particles is now present in all answers
				var particles_array = object.user_particles;	
				var one = particles_array[0];
				var two = particles_array[1];
				var three = particles_array[2];
				var four = particles_array[3];
				var sum = one + two + three + four;	
				messagesReceivedParticleNumber.innerHTML = '<li class="received"><span>Received:</span>' + particles_array + '</li>';    
				
				{//report
				if(object.response == "report"){//block if too many particles (check status and react if there are too many particles)
					particle_counter_one = one;
					particle_counter_two = two;
					particle_counter_three = three;
					particle_counter_four = four;
					BlockCheck(one, two, three, four);
					UnblockCheck(one, two, three, four);
				}
				}//report
				
				//release
// 				if(object.response == "release" && !too_much_one){release_one = true;}//release
// 				if(object.response == "release" && !too_much_two){release_two = true;}//release	
// 				if(object.response == "release" && !too_much_three){release_three = true;}//release
// 				if(object.response == "release" && !too_much_four){release_four = true;}//release

// 				//info text bars showing particle numbers
// 				svg_text_info_EN = "Particles           " + particles_array;
// 				svg_text_info_DE = "Particles           " + particles_array; 
			};

			// Show a disconnected message when the WebSocket is closed.
			socket.onclose = function(event) {
				socketStatus.innerHTML = 'Disconnected from WebSocket.';
				socketStatus.className = 'closed';
			};

			// Close the WebSocket connection when the close button is clicked.
			closeBtn.onclick = function(e) {
				e.preventDefault();
				socket.close();// Close the WebSocket.
				return false;
			};
		}
		WebsocketStart();
			
		//check connection every 4 seconds and reconnect if not connected
		var ws_checkinterval = 4000;
		function check_websocket() {
			if (!socket || socket.readyState == WebSocket.CLOSED){//if (!socket || socket.readyState == 3){
				console.log("websockets", socket);
				WebsocketStart();
			};
		}
		setInterval(check_websocket, ws_checkinterval);	
		