QT += webkit
SOURCES = qtwebkit_new.cpp
# QT += widgets
QT += webkitwidgets


####How to build and run qtwebkit:

  #libraries needed:
    #sudo apt-get install qt-sdk libqt5webkit5-dev

  #compile:
    #qmake qtwebkit.pro
    #make

  #run (can also use the full file path file:///home/user/WebGL/webgl_GalaxyCollision.html):
    # ./qtwebkit -l ../webgl_GalaxyCollision.html -z 0.6
    
  #run with Bonsai (if html name is changed)  
    #change Server/cfg.py 
    #"gui": { "cmd": "../WebGL/webkit/qtwebkit -l ../WebGL/webgl_GalaxyCollision.html".split(),
