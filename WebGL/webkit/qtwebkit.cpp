#include <QApplication>
#include <QWebView>
  
int main(int argc, char** argv) {
    QApplication app(argc, argv);
    QWebView view;

    //transparency
    view.setStyleSheet("background:transparent");
    view.setAttribute(Qt::WA_TranslucentBackground);

    view.show();
    view.setUrl(QUrl("file:///home/user/WebGL/webgl_GalaxyCollision.html"));
    view.showFullScreen();
    view.setZoomFactor(0.6);    
    return app.exec();
}