#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QDir>
#include <QWebView>
#include <QDesktopWidget>

#include <QWebInspector>
  
int main(int argc, char** argv) {

    QApplication app(argc, argv);

    //The following argument parsing is due to https://forum.qt.io/post/259890
    QCoreApplication::setApplicationName("Transparent QtWebKit Browser");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Description: runs the specified web application in a transparrent window");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption URL(QStringList() << "l" << "url",
                           QCoreApplication::translate("main", "URL of web application to run (mandatory argument)"),
                           QCoreApplication::translate("main", "url"));
    parser.addOption(URL);

    QCommandLineOption ZOOM(QStringList() << "z" << "zoom",
                           QCoreApplication::translate("main", "Zoom factor (optional argument, default: 0.6)"),
                           QCoreApplication::translate("main", "zoom"), "0.6");
    parser.addOption(ZOOM);

    
    parser.process(app);

    
    bool isNumericZoom = false;
    qreal _zoom = parser.value(ZOOM).toDouble(&isNumericZoom);

    if (!isNumericZoom) {
        fprintf(stderr, "%s\n", qPrintable(QCoreApplication::translate("main", "Error: Zoom must be a number.")));
        parser.showHelp(1);
    }
    if (!parser.isSet(URL)) {
        fprintf(stderr, "%s\n", qPrintable(QCoreApplication::translate("main", "Error: Must specify an URL.")));
        parser.showHelp(1);
    }

    QUrl _url(parser.value(URL), QUrl::StrictMode);

    if (_url.scheme().isEmpty())
    {
        _url.setUrl("file://" + QDir::current().absoluteFilePath(_url.toString())); 
    }
    if (!_url.isValid()) {
        fprintf(stderr, "%s\n", qPrintable(QCoreApplication::translate("main", "Error: Invalid URL agument!")));
        parser.showHelp(1);
    }
 
    QWebView view;
      
    
    //inspector (inspect on right click)
    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
//  QWebInspector inspector;

    //font
//  QWebSettings::globalSettings()->QWebSettings::setFontFamily(QWebSettings::SansSerifFont, "HelveticaNeueLT Com 45 Lt");//"HelveticaNeueLT Com 45 Lt" Helvetica Neue LT Com
//  QWebSettings::globalSettings()->QWebSettings::setFontSize(QWebSettings::DefaultFontSize, 22);
    
    //transparency
    view.setStyleSheet("background:transparent");
    view.setAttribute(Qt::WA_TranslucentBackground);

    view.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    
    //QUrl("file:///home/user/WebGL/webgl_GalaxyCollision.html"));
    view.load(_url);
    view.resize(app.desktop()->size());
   
    view.showFullScreen();
    
    view.setZoomFactor(_zoom);

    view.show(); 
    view.setUrl(_url);

    return app.exec();
}
