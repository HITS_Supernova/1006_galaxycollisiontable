# 1006 - Galaxy Collision

## Short Description

<table align="center">
    <tr>
    <td align="left" style="font-style:italic; font-size:12px; background-color:white">Let galaxies collide!<br>
        Pick a corner and slowly drag a galaxy onto the table. <br>
        See how it interacts with other galaxies close to it. <br>
        Let them collide and merge!</td>
    </tr>
</table>

The application shows how galaxies are interacting with each other. Galaxies
contain a huge number of stars, each having its own mass and velocity. The
evolution of such galaxies is computed using the science code *Bonsai* and
displayed in real-time. Galaxies can be added and removed interactively. This
allows the user to interactively explore how galaxies interact. The simulation
includes both the stars of the model galaxies as well as the invisible "dark
matter".

In the Supernova exhibition the application is used on a touchtable. Twelve
galaxies can be chosen from the rotating corner menu by pressing the next or
previous button. The text bar explains what kind of a galaxy will be used. The
simulation of the chosen galaxy starts when a pad is pulled into space. The
player's galaxies (or galaxy remnants) can be removed from the simulation by
pressing the home button. A limited number of 50,000 stars is available for
each of the four players to use. If this number is exceeded the stars need to
be removed. The info button gives information about the current number of used
stars.

This application is used at the [ESO Supernova Planetarium and Visitor
Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more
details about the project and how applications are run and managed within the
exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).  


## Requirements / How-To

This station uses a touch table (but any touchscreen will do) and can be used by four people at once.  
Application is composed out of:

* the browser GUI control (`WebGL` folder) which uses a transparent browser (`WebGL/webkit` folder) handles user interaction ("webgl" naming is only used for historical reasons),
* the n-body simulation code *Bonsai* (`Bonsai` folder) that simulates the interaction of the galaxies, and
* the communication server (`Server` folder) takes care of the communication between the two components. 

If no touch input is available, the interactive can be operated with the mouse,
but `MEL=true` needs to set in 'WebGL/parameters.js' (otherwise the menu won't
respond).

## Detailed Information 

#### Galaxy Data

The `galaxy_data` folder contains a set of 12 galaxies of various types, masses
and sizes. They were extracted from the TNG100 cosmological simulation of the
[Illustris TNG project](http://www.tng-project.org) at present time (z = 0) and
contain both dark matter and star particles. The selection is arbitrary, but
meant to include a variety of simulated galaxies without any goal of being
representative.

We note that in present-day n-body simulations, model galaxies still have much
smaller particle numbers than galaxies in nature (having hundreds of billions
for Milky Way sized galaxies). This means that the each "star particle"
actually models a large number of actual stars, and accordingly the mass of a
star particle is much larger than any physical stellar mass. To achieve a good
frame rate in our application, we have to limit the number of star particles
further (here: to 200 000 particles in total). While this results in less
accurate results compared to science simulations, it is still able to reproduce
the overall galaxy behavior and properties of the galaxy - galaxy interactions.
Yet, in general, the more limiting factor is the lack of modeling of the
galactic gas component, but the required hydrodynamical model would simply be
prohibitive for our real-time interaction - that is the compromise we had to
make.

The galaxies were rotated and colors were assigned to particles to highlight
color structures and features in the data, but not meant to yield realistic
colors (which also would be very hard because the additive blending of colors
in Bonsai/OpenGL is quite different to the popular approach of coloring
logarithmically-scaled RGB luminosity sums). To keep the total number of
particles low enough for good realtime rendering in the application, only every
5th particle of the original data was used and then saved in tipsy-formatted
file (`test/tipsy/ascii_to_tipsy.cpp` of our Bonsai submodule with a few color
processing variations). These files are read by Bonsai on application startup
and then also are subject to some OpenGL color magic inside of Bonsai.  The
subdirectory `galaxy_data/available/` contains the ready-to-use tipsy files
(`galaxy_type_*.tipsy`), while the original particles data are available in
`galaxy_data/original/`.

Galaxies used in the application:

1. Large Elliptical Galaxy (24879 particles, `galaxy_type_0.tipsy`, from ID 499)
2. Large Spiral Galaxy with companions (27368 particles, `galaxy_type_1.tipsy`, from ID 633)
3. Large Spiral Galaxy (30425 particles, `galaxy_type_2.tipsy`, from ID 685)
4. Large Spiral Galaxy (22181 particles, `galaxy_type_3.tipsy`, from ID 812)
5. Middle-sized Disk Galaxy (13034 particles, `galaxy_type_4.tipsy`, from ID 2171)
6. Middle-sized Disk Galaxy with several companions (13211 particles, `galaxy_type_5.tipsy`, from ID 1855)
7. Middle-sized Disk Galaxy with a companion (14044 particles, `galaxy_type_6.tipsy`, from ID 2058)
8. Small Disk Galaxy (6575 particles, `galaxy_type_7.tipsy`, from ID 3683)
9. Small Disk Galaxy (6679 particles, `galaxy_type_8.tipsy`, from ID 3530)
10. Small Disk Galaxy with a companion (6627 particles, `galaxy_type_9.tipsy`, from ID 2828)
11. Small Elliptical Galaxy (6233 particles, `galaxy_type_10.tipsy`, from ID 5740)
12. Tiny Disk Galaxy (3351 particles, `galaxy_type_11.tipsy`, from ID 8063)


#### Bonsai

The science code [Bonsai](https://github.com/treecode/Bonsai) calculates the
gravitational interaction of particles with CUDA on a Nvidia GPU and renders
the galaxies via OpenGL. Both star particles and dark matter particles are
included in the computation. Since Bonsai is targeted at scientific
simulations, it did not have the capability of interacting with the user and
permit addition and removal of galaxies at runtime. For our application, this
functionality has been added (Credit: Bernd Doser) and now allows interactive
changes to the simulation, listening on a TCP port and accepting ASCII
commands.

[Our changes to Bonsai](https://github.com/vga101/Bonsai/tree/eso-supernova)
furthermore include various tweaks to the OpenGL rendering so that galaxies are
rendered nicely and their structural peculiarities are visible. However, the
coloring is not aimed at yielding realistic colors for the model galaxies.

At ESO Supernova, the simulation runs on a Nvidia GTX 1080 graphics card and
frame rates for smooth rendering made a restriction to 200,000 particles (stars
and dark matter) necessary. Accordingly, each of the maximum 4 user has 50,000
particles at their disposal.

`prepare.sh` builds Bonsai from the sources and has the resulting executable at
`runtime/build/bonsai2_slowdust`. It should be run with parameters like
`runtime/build/bonsai2_slowdust --war-of-galaxies ../galaxy_data/available/
--port 50008 -i ../galaxy_data/dummy-color.tipsy --fullscreen --camera-distance
250`. For our application, the actually used parameters are defined by the
communication server (in `cfg.py`).


#### Server

The server component takes care of starting the necessary components (GUI and
Bonsai), relays messages between them, monitors their health and reports the
health status of the overall application back to the Hilbert management system
as "heartbeat".

Configuration is done in `cfg.py` and `start.sh` starts the server.


#### Webkit: How to build and run the transparent browser (using qtwebkit):

A transparent browser application is used to run the GUI web page.  
It can be found here: `WebGL/webkit/qtwebkit_new.cpp`

      * libraries needed: qt-sdk, libqt5webkit5-dev
      * to compile: 
            qmake qtwebkit.pro  
            make   
      * to run (can also use the full file path *file:///.../webgl_GalaxyCollision.html*):   
            ./qtwebkit -l ../webgl_GalaxyCollision.html -z 0.6 
      * to run with full application (if html name has changed) change *Server/cfg.py*   
            "gui": { "cmd": "../WebGL/webkit/qtwebkit -l ../WebGL/webgl_GalaxyCollision.html".split(), ...


#### GUI, Bonsai: Client / Server communication

The GUI communicates via WebSockets and Bonsai uses a plain TCP socket for
ASCII message. The communication server translates between the two, messages
are in JSON format. We use three tasks for communication: `release`, `remove`
and `report`. 
   
    port: 50008 (Bonsai <-> Python)  
    port: 8881  (Python <-> GUI)
  
    WebSockets example messages:  
        send from GUI:   
            {"task":"release","galaxy_id":M,"user_id":N,"position":[x,y],"velocity":[x,y]}  
            {"task":"remove","user_id":N}  
            {"task":"report"}  
        GUI receive:  
            {"response":"release","simulation_time":T,"user_particles":[N_P1,N_P2,N_P3,N_P4]}  
            {"response":"remove", "simulation_time":T,"user_particles":[N_P1,N_P2,N_P3,N_P4]}  
            {"response":"report", "simulation_time":T,"user_particles":[N_P1,N_P2,N_P3,N_P4]}     

Note when viewing the websockets output in the GUI:  Report is not printed out
when it is sent using `"task":"report"` because it clutters the screen (text gets added),
but it is the only thing visible when it is received using `"response":"report"` because
it gets printed out quickly in the animation loop (text gets replaced).


#### (Optional) Using the Additional Scripts

The application is run in a docker container within the
[Hilbert](https://github.com/hilbert) Museum Management System. Our local
docker-compose file starts `Server/start.sh` to get all the application
running.

Application uses the following scripts to start: 

* `docker-compose.hitsgalaxycollision.yml` (yml file not available):
    * starts the python server 'Server/start.sh'  
        * handles antialiasing, transparency, heartbeat
        * hides the mouse cursor
        * starts 'Server/server.py'
        
* `Dockerfile` additionally does the following:
    * prepares the Bonsai `prepare.sh`
    * compiles the transparent web browser in the 'WebGL/webkit' folder

Although our Dockerfile uses our `hits/hitsfat` base image, CUDA capability is
also provided by the `hilbert/cuda_devel` docker image of Imaginary.


## Credits

* This application was developed by the ESO Supernova team at [HITS gGmbH](https://www.h-its.org/en/).   
* Idea by Kai Polsterer, Volker Gaibler and Dorotea Dudas.  
* GUI control application by Dorotea Dudas. 
* Server code by Volker Gaibler.  
* Bonsai code extension "War Of Galaxies" to allow interactive galaxy addition or removal implemented by Bernd Doser.  
* Additional changes to Bonsai by Volker Gaibler and Dorotea Dudas.   
* Selected galaxies extracted from the [Illustris TNG](http://www.tng-project.org) cosmological simulation 
  ([Pillepich A. et al., 2018, MNRAS, 475, 648](http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1707.03406)
  and references therein), kindly provided Rüdiger Pakmor.
* We are grateful to Jeroen Bédorf for his help with the initial setup.


#### Code Licensing

* This repository is licensed as: [MIT license](LICENSE)
* *Bonsai* n-body simulation code by Jeroen Bédorf and Evghenii Gaburov, [Bonsai website](https://github.com/treecode/Bonsai) (Apache License 2.0)
* MIT license: 
    * *jQuery* [source](https://jquery.com/)
    * *interact.js* by Taye Adeyemi [source](http://interactjs.io/)


#### Image Licensing

* CC BY 4.0: 
    * GUI images by Dorotea Dudas

