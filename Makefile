mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
IMAGE_VERSION ?= v1.1

# site-specific settings
include ../0000_general/Hilbert/Makefile-local.inc

U = hits
APP = galaxycollision

# default variables and targets
include ../0000_general/Hilbert/hilbert-docker-images/images/Makefile.inc

CMD=bash
check: $(TS)
	export IMAGE_VERSION=${IMAGE_VERSION}
	$(COMPOSE) -p $(COMPOSE_PROJECT_NAME) run --rm ${APP} ${CMD}
	unset IMAGE_VERSION

