FROM hits/hitsfat:0.3.0-1

ARG DP_CREDENTIALS=none

# Bonsai:
COPY Bonsai /I/Bonsai/
COPY galaxy_data /I/galaxy_data/
RUN cd /I/Bonsai \
    && ./prepare.sh 2>&1 | tee prepare.out

# GUI
COPY WebGL /I/WebGL/
ENV QT_SELECT=5
RUN cd /I/WebGL/webkit \
    && rm -f qtwebkit Makefile *.o \
    && qmake qtwebkit.pro \
    && make

# Python server
COPY Server /I/Server/
COPY hilbert-heartbeat /I/hilbert-heartbeat/

WORKDIR /I
ENTRYPOINT [ "/sbin/my_init", "--skip-runit", "--skip-startup-files", "--"]
CMD ["/I/Server/start.sh"]


ARG IMAGE_VERSION=latest
ARG GIT_NOT_CLEAN_CHECK
ARG BUILD_DATE=unknown
ARG VCS_REF=unknown
ARG VCS_DATE=unknown
ARG VCS_SUMMARY=unknown
ARG VCS_URL=unknown
ARG DOCKERFILE=unknown

LABEL maintainer="Volker Gaibler <volker.gaibler@h-its.org>" \
    org.label-schema.name="Galaxy Collision" \
    org.label-schema.description="Interactive application for the ESO Supernova" \
    org.label-schema.vendor="HITS gGmbH" \
    org.label-schema.vcs-ref="${VCS_REF}" \
    org.label-schema.vcs-url="${VCS_URL}" \
    org.label-schema.version="${VCS_VERSION}" \
    org.label-schema.build-date="${BUILD_DATE}" \
    org.label-schema.schema-version="1.0" \
    VCS_DATE="${VCS_DATE}" \
    VCS_SUMMARY="${VCS_SUMMARY}" \
    IMAGE_VERSION="${IMAGE_VERSION}" \
    GIT_NOT_CLEAN_CHECK="${GIT_NOT_CLEAN_CHECK}" \
    DOCKERFILE="${DOCKERFILE}"
